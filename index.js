const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cookieParser = require('cookie-parser')
const cloudinary = require('cloudinary').v2
const fileupload = require('express-fileupload')
const base64Img = require('base64-img')

const fs = require('fs')
const cors = require('cors')

require('dotenv').config()

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUDPOINT_API_KEY,
    api_secret: process.env.CLOUDPOINT_API_SECRET,
})

/// CORS

const whitelist = ['http://localhost:3000', 'https://mayo-4215.herokuapp.com']

const corsOptions = {
    credentials: true,
    origin: (origin, callback) => {
        return callback(null, true)
        if (whitelist.includes(origin)) return callback(null, true)

        callback(new Error('Not allowed by CORS'))
    },
    optionsSuccessStatus: 200,
    methods: 'GET, POST, OPTIONS',
}

const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

mongoose.Promise = global.Promise
mongoose.connect(process.env.DATABASE, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
})

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))

app.use(cookieParser())
app.use('/static', express.static(__dirname + '/static'))
app.use(cors(corsOptions))
app.use(
    fileupload({
        useTempFiles: true,
    })
)

//Middleware
const { auth } = require('./middleware/auth')

//Models
const { User } = require('./models/user')
const { Dialog } = require('./models/dialog')

const throwError = (res, errorCode, errorMessage) => {
    return res.status(errorCode).json({
        error: errorMessage,
    })
}
//=====================================
//              USERS
//=====================================

app.post('/api/users/register', (req, res) => {
    const newUser = new User(req.body)
    User.findOne({ email: req.body.email }, (err, user) => {
        if (user) {
            throwError(
                res,
                403,
                'A user with this email already exists. Try other email!'
            )
        } else {
            newUser.save((err, doc) => {
                if (err) throwError(res, 403, err)
                res.status(200).json({
                    userdata: doc,
                })
            })
        }
    })
})

app.post('/api/users/login', (req, res) => {
    // find email
    User.findOne({ email: req.body.email }, (err, user) => {
        if (!user || err) {
            throwError(res, 403, 'Email not found. Try again.')
        } else {
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (!isMatch || err) {
                    throwError(res, 403, 'Wrong password. Try again.')
                } else {
                    user.generateToken((err, user) => {
                        if (err) throwError(res, 403, err)

                        res.cookie('user_token', user.token).status(200).json({
                            token: user.token,
                            id: user._id,
                        })
                    })
                }
            })
        }
    })
})

app.post('/api/users/auth', auth, (req, res) => {
    res.status(200).json({
        isAuth: true,
        id: req.user._id,
        avatarUrl: req.user.avatarUrl,
        name: req.user.name,
        email: req.user.email,
        lastname: req.user.lastname,
        contacts: req.user.contacts,
        theme: req.user.theme
    })
})

app.post('/api/users/addAvatar', (req, res) => {
    const { file } = req.files

    cloudinary.uploader.upload(
        file.tempFilePath,
        {
            transformation: {
                width: 200,
                height: 200,
                gravity: "face",
                crop: "thumb"
            },
        },
        function (err, result) {
            if (err) {
                res.status(413).json({
                    code: 413,
                    error: err,
                })
            } else {
                res.status(200).json(result.url)
            }
        }
    )
})

app.post('/api/users/saveAvatar', (req, res) => {
    User.findOne({ _id: req.body }, (err, user) => {
        if (user) {
            if (user.avatarUrl) {
                const splittedUrl = user.avatarUrl.split('/')
                const publicId = splittedUrl[splittedUrl.length - 1].split(
                    '.'
                )[0]

                cloudinary.uploader.destroy(publicId, function (err) {
                    if (err) {
                        res.status(404).json({
                            status: 404,
                            error: 'Image not found',
                        })
                    }
                })
            }
            user.avatarUrl = req.body.avatarUrl
            user.save((err, doc) => {
                res.status(200).json({
                    userData: doc,
                })
            })
        } else {
            throwError(res, 401, 'Cannot find user.')
        }
    })
})

app.post("/api/users/setTheme", (req, res) => {
    User.findOne({ _id: req.body }, (err, user) => {
        if(user) {
            user.theme = req.body.theme
            user.save((err, user) => {
                res.status(200).json({theme: user.theme})
                if(err) {
                    throwError(res, 401, 'Please try again.')
                }
            })
        } else {
            throwError(res, 401, 'Cannot find user.')
        }
    })
})

app.get('/api/users/getContactById', (req, res) => {
    const { contactId } = req.query
    User.findOne({ _id: contactId }, (err, user) => {
        if (user) {
            const { name, lastname, avatarUrl, email, _id } = user
            res.status(200).json({
                id: _id,
                name,
                lastname,
                email,
                avatarUrl,
            })
        } else {
            throwError(res, 401, 'Cannot find user.')
        }
    })
})

app.get('/api/users/getDialogById', (req, res) => {
    const { dialogId } = req.query

    Dialog.findOne({ _id: dialogId }, (err, dialog) => {
        if (dialog) {
            res.status(200).json(dialog)
        } else {
            throwError(res, 401, 'Cannot find dialog.')
        }
    })
})

app.post('/api/users/updateClient', (req, res) => {
    const { id } = req.body
    User.findOne({ _id: id }, (err, user) => {
        if (user) {
            for (key in req.body) {
                if (key !== '_id' && req.body[key] !== '') {
                    user[key] = req.body[key]
                }
            }
            user.save((err, doc) => {
                res.status(200).json({
                    userData: doc,
                })
            })
        } else {
            throwError(res, 401, 'Cannot find user.')
        }
    })
})

//=====================================
//              SOCKET
//=====================================
connections = []

function numClientsInRoom(namespace, room) {
    const clients = io.nsps[namespace].adapter.rooms[room];
    return Object.keys(clients).length;
  }

io.sockets.on('connection', (socket) => {
    connections.push(socket)
    socket.on('disconnect', () => {
        connections.splice(connections.indexOf(socket), 1)
    })

    socket.on('joinChat', (dialogId) => {
        socket.join(dialogId)
    })
    socket.on('leaveChat', (dialogId) => {
        socket.leave(dialogId)
    })

    socket.on('sendMessage', (dialogMessage) => {
   
        let socketsInRoom = 0
        for (key in socket.rooms) {
            if (key) {
                socketsInRoom++
            }
        }

        if (socketsInRoom < 2) {
            const filteredSocket = connections.find(
                (socket) =>
                    socket.handshake.query.id === dialogMessage.contactId
            )
            if (filteredSocket) {
                socket.broadcast
                    .to(filteredSocket.id)
                    .emit('inviteToChat', dialogMessage.id)
            }
        }
        saveMessageToHistory(dialogMessage, socket)
     
        socket.broadcast
            .to(dialogMessage.dialogId)
            .emit('receiveMessage', dialogMessage)
    })

    socket.on('sendFile', (dialogMessage) => {
     
        let socketsInRoom = 0
        for (key in socket.rooms) {
            if (key) {
                socketsInRoom++
            }
        }

        if (socketsInRoom < 2) {
            const filteredSocket = connections.find(
                (socket) =>
                    socket.handshake.query.id === dialogMessage.contactId
            )
            if (filteredSocket) {
                socket.broadcast
                    .to(filteredSocket.id)
                    .emit('inviteToChat', dialogMessage.id)
            }
        }
        
        socket.broadcast
        .to(dialogMessage.dialogId)
        .emit('receiveFile', dialogMessage)


        saveFile(dialogMessage, socket)
    })
})

const saveFile = (message, socket) => {
    const filepath = base64Img.imgSync(message.imageUrl, __dirname + '/static/', message.fileName, function (err, filepath) {
        if (err) {
            socket.emit("error", err)
            return
        }
    })
    message.imageUrl = ''
    cloudinary.uploader.upload(
        filepath,
        {
            transformation: {
                width: 300,
                height: 300
            },
        },
        function (err, result, socket) {
            if (err) {
                socket.emit("error", err)
            } if (result) {
                Dialog.findOne({
                    _id: message.dialogId,
                }).exec((err, dialog, socket) => {
                    if (dialog) {
                        message.imageUrl = result.url
                        dialog.images.push(result.url)
                        dialog.messages.push(message)
                        dialog.save((err, doc) => {
                            if (err) {
                                socket.emit("error", err)
                            }
                        })
                    }
                })
            }
            fs.unlinkSync(filepath)
        }
    )
}

const saveMessageToHistory = (dialogMessage, socket) => {
    Dialog.findOne({
        _id: dialogMessage.dialogId,
    }).exec((err, dialog) => {
        if (dialog) {
            dialog.messages.push(dialogMessage)
            dialog.save((err, doc) => {
                if (err) {
                    socket.emit("error", err)
                }
            })
        }
    })
}

app.get('/api/users/getGallery', (req, res) => {
    const { dialogId } = req.query

    Dialog.findOne({ _id: dialogId }, (err, dialog) => {
        if (dialog) {
            res.status(200).json(dialog.images)
        } else {
            throwError(res, 401, 'Cannot find dialog.')
        }
    })
})
app.post('/api/users/findContact', (req, res) => {
    // find email
    const { email } = req.body
    User.findOne({ email: email }, (err, user) => {
        if (!user || err) {
            throwError(res, 403, 'Email not found. Try again.')
        } else {
            res.status(200).json({
                id: user._id,
                avatarUrl: user.avatarUrl,
                name: user.name,
                email: user.email,
                lastname: user.lastname,
            })
        }
    })
})

app.post('/api/users/addContact', (req, res) => {
    const { clientId, contactId } = req.body
    User.find({
        _id: {
            $in: [
                mongoose.Types.ObjectId(clientId),
                mongoose.Types.ObjectId(contactId),
            ],
        },
    }).exec((err, docs) => {
        const user = docs.find((doc) => doc.id === clientId)
        const contact = docs.find((doc) => doc.id === contactId)
        if (user && contact) {
            const isAlreadyContact = user.contacts.some(
                (contact) => contact.contactId === contactId
            )
            if (!isAlreadyContact) {
                Dialog.findOne()
                    .where({
                        participants: {
                            $all: [clientId, contactId],
                        },
                    })
                    .exec((err, dialog) => {
                        if (!dialog) {
                            const dialog = new Dialog({
                                messages: [],
                                participants: [clientId, contactId],
                            })

                            const dialogId = dialog.id
                            user.contacts.push({
                                contactId,
                                dialogId,
                            })
                            contact.contacts.push({
                                contactId: clientId,
                                dialogId,
                            })
                            dialog.save((err, dialogdoc) => {
                                if (err) return res.json(err)
                            })
                        } else {
                            const dialogId = dialog.id
                            user.contacts.push({
                                contactId,
                                dialogId,
                            })
                        }
                        user.save((err, userdoc) => {
                            if (!err) {
                                contact.save((err, contact) => {
                                    if (!err) {
                                        const {
                                            email,
                                            lastname,
                                            name,
                                            avatarUrl,
                                            _id,
                                        } = contact

                                        res.status(200).json({
                                            email,
                                            lastname,
                                            name,
                                            avatarUrl,
                                            id: _id,
                                        })
                                    } else {
                                        throwError(res, 401, err)
                                    }
                                })
                            } else {
                                throwError(res, 401, err)
                            }
                        })
                    })
            }
        } else {
            throwError(res, 401, err)
        }
    })
})

app.post('/api/users/deleteContact', (req, res) => {
    const { clientId, contactId } = req.body
    User.findOne({ _id: clientId }).exec((err, user) => {
        const contact = user.contacts.find(
            (contact) => contact.contactId === contactId
        )
        if (contact) {
            user.contacts.remove(contact)
            user.save((error, user) => {
                res.status(200).json(user.contacts)
                if (error) {
                    throwError(res, 401, error)
                }
            })
        }
    })
})
//=====================================
//              DIALOG
//=====================================

app.post('/api/messages/addMessage', (req, res) => {
    const { dialogId, message } = req.body
    Dialog.findOne({
        _id: { $in: mongoose.Types.ObjectId(dialogId) },
    }).exec((err, dialog) => {
        if (dialog) {
            dialog.messages.push(JSON.stringify(message))
            dialog.save((err, doc) => {
                res.status(200).json({
                    doc,
                })
            })
        } else {
            throwError(res, 403, err)
        }
    })
})

const port = process.env.PORT || 3006

server.listen(port, () => {
    console.log(`Server runnig at ${port}`)
})
